#include<iostream>
#include <string>
#include <cmath>
int main() {

    int numberofcards = 104;
    std::string twopacks[104];
    int index = 0;
    for(int n = 1; n <=2; n++) {
        for(int j = 1; j <= 4; j++) {
        for(int i = 1; i <= 13; i++) {
                if(j == 1) {
                    
                    twopacks[index] =  std::to_string(i) + "♠";
                     
                }else if(j == 2) {
                     
                        twopacks[index] = std::to_string(i) +"♥" ;
                    
                } else if(j == 3) {
                        twopacks[index] = std::to_string(i)+"♣"  ;
                } else if(j == 4) {  
                    twopacks[index] = std::to_string(i)+ "♦";
                }
                index++;
            }
        }
    }
    
    bool positions[104];
    for (int m = 0; m < 104; m++) {
        positions[m] = false;
    }
  
    std::string hand[5] = {"","","","",""};
    std::cout << "This is pocker!"<<std::endl;
    std::cout<<"This is your hand:"<<std::endl;
    while(numberofcards >= 5) {
        int cardCount = 5;   
        while(cardCount >= 1) {
            int randomNumber = rand() % 104 + 1;
            while(positions[randomNumber-1]== true) {
                 randomNumber = rand() % 104 + 1;
            }
            if(randomNumber <=104 && randomNumber > 0) {
                std::cout << twopacks[randomNumber-1]<<std::endl;
                hand[cardCount-1] = twopacks[randomNumber-1];
                positions[randomNumber-1] = true;
                cardCount--;  
            }
        }
        std::cout<<std::endl;
        std::string handResult = "";
        int numberhand[5];
        int samecards[13] = {0,0,0,0,0,0,0,0,0,0,0,0,0};
        int samecolor[4];
        int following_card =0;
        std::string highest="2♠";
        //hand in numbers
        for(int i = 0; i <5; i++) {
            numberhand[i]= std::stoi(hand[i]);
        }
        //highest
        int highestint =0;
        for(int i = 0; i <5; i++) {
            for(int j = 0; j < 5;j++) {
                if(numberhand[i]>= highestint && numberhand[i] != 1){
                    highest = hand[i];
                    highestint = numberhand[i];
                } else if (numberhand[i] == 1) {
                    highest =  hand[i];
                    break;
                }
            }
        }
    //same cards
        for (int j = 0; j < 5; j ++) {
            for(int k = 0; k <5; k++) {
                int num = samecards[numberhand[j]-1];
                if( numberhand[j]== numberhand[k] && j!=k) {
                    samecards[numberhand[j]-1]++;
                }

            }
        
        }
        
    
    //colors
        for(int j = 0; j < 5; j ++) {
            for(int k = 0; k <5; k++) {
                if(k==j) {
                    continue;
                } else if (hand[j].length() == 2 && hand[j].length() == 2 &&hand[j].at(1)==hand[k].at(1)) {
                    if(hand[j].at(1)=='♠') {
                        samecolor[0] = samecolor[0] +1;
                    } else if(hand[j].at(1)=='♥') {
                        samecolor[1] = samecolor[1] +1;
                    } else if(hand[j].at(1)== '♣' ) {
                        samecolor[2] = samecolor[2] +1;
                    } else if(hand[j].at(1)== '♦') {
                        samecolor[3] = samecolor[3] +1;
                    }
                } else if(hand[j].length() == 3 && hand[j].length() == 3 &&hand[j].at(2)==hand[k].at(2)) {
                     if(hand[j].at(2)=='♠') {
                        samecolor[0] = samecolor[0] +1;
                    } else if(hand[j].at(2)=='♥') {
                        samecolor[1] = samecolor[1] +1;
                    } else if(hand[j].at(2)== '♣' ) {
                        samecolor[2] = samecolor[2] +1;
                    } else if(hand[j].at(2)== '♦') {
                        samecolor[3] = samecolor[3] +1;
                    }
                }
            }
        }
        
        bool stright = false;
        for(int j = 1; j < 5; j ++) {
            int countoffollowings =0;
            int previous = numberhand[j];
            for(int k = 0; k <5; k++) {
                if (previous +1 ==numberhand[k] && j!=k) {
                    previous = numberhand[k];
                    countoffollowings++;
                }
            }
            if (countoffollowings == 5) {
                stright = true;
            }
        }
    //same cards
        int pairs = 0;
        int fullhouse = 0;
        for(int i = 0; i < 13; i++) {
            for(int j = 4; j > 0; j--) {
                 if(samecards[i]== 8) {
                      handResult = "Four of kind";
                 } else if(samecards[i]== 6) {
                     handResult = "Three of kind";
                    fullhouse++;
                } else if(samecards[i]== 2) {
                    handResult = "One Pair";
                    pairs++;
                    fullhouse++;
                } 
            }
            if(pairs == 2) {
                handResult = "Two Pairs";
            }
            if(fullhouse==2) {
                handResult = "Full House";
            }
        }
        int colors =0;
        for(int i =0; i < 4; i++) {
            if(samecolor[i]==5) {
                handResult = "Flush";
                colors = 1;
            }
        }
        if(stright==true) {
            handResult = "Stright";
        }
        if (colors=1 && stright == true) {
             handResult = "Stright Flush";
        }
        if(handResult == "") {
            handResult = "High card " + highest;
        }

        std::cout <<handResult <<std::endl;
        if(numberofcards >= 5) {
            std::cout << "Want to play again? There is still cards on deck. Hit enter to play."; 
            do {
                std::cout << '\n' << "Press an enter to get a new game..."<<std::endl;
            } while (std::cin.get() != '\n');
        }
    }
}

    